/**
 * \file CompteTesteur.cpp
 * \brief Implantation des tests unitaires de la classe Compte
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-03-30
 */

#include <gtest/gtest.h>
#include "Compte.h"

using namespace std;
using namespace banque;

/**
 * \class CompteDeTest
 * \brief Classe de test permettant de tester la classe abstraite Compte
 */
class CompteDeTest: public Compte
{
public:
	CompteDeTest(int noCompte, double tauxInteret, double solde, const std::string& description) :
		Compte(noCompte, tauxInteret, solde, description) {};

	virtual double calculerInteret() const
	{
		return 0;
	};

	virtual Compte* clone() const
	{
		return 0;
	};
};

/**
 * \brief Test du constructeur
 * 		 	Cas valides : vérification d'assignation de tous les attributs.
 * 		  	Cas invalides :
 * 		  		Numéro de compte = 0
 * 		  		Numéro de compte < 0
 * 		  		Taux d'intérêt = 0
 * 		  		Taux d'intérêt < 0
 * 		  		Description vide
 */
TEST(Compte, Constructeur)
{
	CompteDeTest unCompte(1572, 1.5, 3500.67, "USD");
	ASSERT_EQ(1572, unCompte.reqNoCompte());
	ASSERT_EQ(1.5, unCompte.reqTauxInteret());
	ASSERT_EQ(3500.67, unCompte.reqSolde());
	ASSERT_EQ("USD", unCompte.reqDescription());
}
//Numéro de compte invalide
TEST(Compte, ConstructeurNoCompteNul)
{
	ASSERT_THROW(CompteDeTest unCompte(0, 1.5, 3500.67, "USD"), ContratException);
}
//Numéro de compte invalide
TEST(Compte, ConstructeurNoCompteNegatif)
{
	ASSERT_THROW(CompteDeTest unCompte(-10, 1.5, 3500.67, "USD"), ContratException);
}
//Taux d'intérêt invalide
TEST(Compte, ConstructeurTauxInteretNul)
{
	ASSERT_THROW(CompteDeTest unCompte(1572, 0.0, 3500.67, "USD"), ContratException);
}
//Taux d'intérêt invalide
TEST(Compte, ConstructeurTauxInteretNegatif)
{
	ASSERT_THROW(CompteDeTest unCompte(1572, -1.5, 3500.67, "USD"), ContratException);
}
//Description invalide
TEST(Compte, ConstructeurDescriptionVide)
{
	ASSERT_THROW(CompteDeTest unCompte(1572, 1.5, 3500.67, ""), ContratException);
}

/**
 * \brief Création d'une fixture à utiliser pour tester les méthodes public de la classe
 */
class unCompte: public ::testing::Test
{
public:
	unCompte():
		compte(3387, 1.6, 10576.77, "étudiant"){}
	CompteDeTest compte;
};

/**
 * \brief Test de la méthode int reqNoCompte() const
 */
TEST_F(unCompte, reqNoCompte)
{
	ASSERT_EQ(3387, compte.reqNoCompte());
}

/**
 * \brief Test de la méthode double reqTauxInteret() const
 */
TEST_F(unCompte, reqTauxInteret)
{
	ASSERT_EQ(1.6, compte.reqTauxInteret());
}

/**
 * \brief Test de la méthode double reqSolde() const
 */
TEST_F(unCompte, reqSolde)
{
	ASSERT_EQ(10576.77, compte.reqSolde());
}

/**
 * \brief Test de la méthode const std::string& reqDescription() const
 */
TEST_F(unCompte, reqDescription)
{
	ASSERT_EQ("étudiant", compte.reqDescription());
}

/**
 * \brief Test de la méthode const util::Date reqDateOuverture() const
 */
TEST_F(unCompte, reqDateOuverture)
{
	ASSERT_EQ(util::Date(), compte.reqDateOuverture());
}

/**
 * \brief Test de la méthode void asgTauxInteret(double p_tauxInteret)
 * 		 	Cas valides : assignation du taux d'intérêt valide.
 * 		  	Cas invalides : taux d'intérêt nul, taux d'intérêt négatif.
 */
TEST_F(unCompte, asgTauxInteretValide)
{
	compte.asgTauxInteret(0.2);
	ASSERT_EQ(0.2, compte.reqTauxInteret());
}
//Taux d'intérêt invalide
TEST_F(unCompte, asgTauxInteretNul)
{
	ASSERT_THROW(compte.asgTauxInteret(0.0), ContratException);
}
//Taux d'intérêt invalide
TEST_F(unCompte, asgTauxInteretNegatif)
{
	ASSERT_THROW(compte.asgTauxInteret(-2.2), ContratException);
}

/**
 * \brief Test de la méthode void asgSolde(double p_solde)
 */
TEST_F(unCompte, asgSolde)
{
	compte.asgSolde(-234.87);
	ASSERT_EQ(-234.87, compte.reqSolde());
}

/**
 * \brief Test de la méthode void asgDescription(const std::string& p_description)
 * 		 	Cas valides : assignation de la description valide.
 * 		  	Cas invalides : assignation de la description vide.
 */
TEST_F(unCompte, asgDescriptionValide)
{
	compte.asgDescription("test");
	ASSERT_EQ("test", compte.reqDescription());
}
//Description invalide
TEST_F(unCompte, asgDescriptionVide)
{
	ASSERT_THROW(compte.asgDescription(""), ContratException);
}

/**
 * \brief Test de la méthode std::string reqCompteFormate() const
 */
TEST_F(unCompte, reqCompteFormate)
{
	std::ostringstream os;
	os << "numero                 : " << compte.reqNoCompte() << endl
			<< "Description            : " << compte.reqDescription() << endl
			<< "Date d'ouverture       : " << compte.reqDateOuverture().reqDateFormatee() << endl
			<< "Taux d'interet         : " << compte.reqTauxInteret() << endl
			<< "Solde                  : " << compte.reqSolde() << " $" << endl;
	ASSERT_EQ(os.str(), compte.reqCompteFormate());
}
