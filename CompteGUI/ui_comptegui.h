/********************************************************************************
** Form generated from reading UI file 'comptegui.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COMPTEGUI_H
#define UI_COMPTEGUI_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CompteGUIClass
{
public:
    QAction *actionSupprimer_un_compte;
    QAction *actionQuitter;
    QAction *actionCheque;
    QAction *actionEpargne;
    QWidget *centralwidget;
    QMenuBar *menubar;
    QMenu *menuMenu;
    QMenu *menuAjouter_un_compte;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *CompteGUIClass)
    {
        if (CompteGUIClass->objectName().isEmpty())
            CompteGUIClass->setObjectName(QString::fromUtf8("CompteGUIClass"));
        CompteGUIClass->resize(800, 600);
        actionSupprimer_un_compte = new QAction(CompteGUIClass);
        actionSupprimer_un_compte->setObjectName(QString::fromUtf8("actionSupprimer_un_compte"));
        actionQuitter = new QAction(CompteGUIClass);
        actionQuitter->setObjectName(QString::fromUtf8("actionQuitter"));
        actionCheque = new QAction(CompteGUIClass);
        actionCheque->setObjectName(QString::fromUtf8("actionCheque"));
        actionEpargne = new QAction(CompteGUIClass);
        actionEpargne->setObjectName(QString::fromUtf8("actionEpargne"));
        centralwidget = new QWidget(CompteGUIClass);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        CompteGUIClass->setCentralWidget(centralwidget);
        menubar = new QMenuBar(CompteGUIClass);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 27));
        menuMenu = new QMenu(menubar);
        menuMenu->setObjectName(QString::fromUtf8("menuMenu"));
        menuAjouter_un_compte = new QMenu(menuMenu);
        menuAjouter_un_compte->setObjectName(QString::fromUtf8("menuAjouter_un_compte"));
        CompteGUIClass->setMenuBar(menubar);
        statusbar = new QStatusBar(CompteGUIClass);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        CompteGUIClass->setStatusBar(statusbar);

        menubar->addAction(menuMenu->menuAction());
        menuMenu->addAction(menuAjouter_un_compte->menuAction());
        menuMenu->addAction(actionSupprimer_un_compte);
        menuMenu->addSeparator();
        menuMenu->addAction(actionQuitter);
        menuAjouter_un_compte->addAction(actionCheque);
        menuAjouter_un_compte->addAction(actionEpargne);

        retranslateUi(CompteGUIClass);
        QObject::connect(actionQuitter, SIGNAL(triggered()), CompteGUIClass, SLOT(close()));

        QMetaObject::connectSlotsByName(CompteGUIClass);
    } // setupUi

    void retranslateUi(QMainWindow *CompteGUIClass)
    {
        CompteGUIClass->setWindowTitle(QApplication::translate("CompteGUIClass", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionSupprimer_un_compte->setText(QApplication::translate("CompteGUIClass", "Supprimer un compte", 0, QApplication::UnicodeUTF8));
        actionQuitter->setText(QApplication::translate("CompteGUIClass", "Quitter", 0, QApplication::UnicodeUTF8));
        actionCheque->setText(QApplication::translate("CompteGUIClass", "Cheque", 0, QApplication::UnicodeUTF8));
        actionEpargne->setText(QApplication::translate("CompteGUIClass", "Epargne", 0, QApplication::UnicodeUTF8));
        menuMenu->setTitle(QApplication::translate("CompteGUIClass", "Menu", 0, QApplication::UnicodeUTF8));
        menuAjouter_un_compte->setTitle(QApplication::translate("CompteGUIClass", "Ajouter un compte", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CompteGUIClass: public Ui_CompteGUIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMPTEGUI_H
