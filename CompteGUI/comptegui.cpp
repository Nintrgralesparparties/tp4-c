#include "comptegui.h"
#include "ajoutchequeinterface.h"
#include "Cheque.h"
#include "CompteException.h"
#include <qmessagebox.h>

using namespace banque;

CompteGUI::CompteGUI(QWidget *parent)
    : QMainWindow(parent)
{
	ui.setupUi(this);
	QObject::connect(ui.actionCheque, SIGNAL(triggered()), this, SLOT(dialogAjoutCheque()));
}

CompteGUI::~CompteGUI()
{

}

void CompteGUI::dialogAjoutCheque()
{
	AjoutChequeInterface saisieCheque(this);
	if (saisieCheque.exec())
	{
		ajoutCheque(saisieCheque.reqNoCompte().toInt(), saisieCheque.reqTauxInteret(), saisieCheque.reqSolde().toDouble(),
				saisieCheque.reqNombreTransactions(), saisieCheque.reqTauxInteretMinimum(),
				saisieCheque.reqDescription().toStdString());
	}
}

void CompteGUI::ajoutCheque(int p_noCompte, double p_tauxInteret, double p_solde,
			int p_nombreTransactions, double p_tauxInteretMinimum,
			const std::string& p_description)
{
	try
	{
		Cheque compteCheque(p_noCompte, p_tauxInteret, p_solde, p_nombreTransactions, p_tauxInteretMinimum, p_description);
	}
	catch (CompteDejaPresentException& e)
	{
		QString message = e.what();
		QMessageBox::information(this, "Erreur", message);
	}
}
