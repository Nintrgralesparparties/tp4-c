TEMPLATE = app
TARGET = CompteGUI
QT += core \
    gui
HEADERS += ajoutchequeinterface.h \
    comptegui.h
SOURCES += ajoutchequeinterface.cpp \
    main.cpp \
    comptegui.cpp
FORMS += ajoutchequeinterface.ui \
    comptegui.ui
RESOURCES += 
LIBS += "../source_TP3/Debug/libsource_TP3.a"
INCLUDEPATH += "../source_TP3/."
