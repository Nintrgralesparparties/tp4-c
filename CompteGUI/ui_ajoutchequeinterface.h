/********************************************************************************
** Form generated from reading UI file 'ajoutchequeinterface.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AJOUTCHEQUEINTERFACE_H
#define UI_AJOUTCHEQUEINTERFACE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_AjoutChequeInterfaceClass
{
public:
    QLabel *label_titre;
    QLineEdit *lineEdit_noCompte;
    QLabel *label_noCompte;
    QLabel *label_tauxInteret;
    QLineEdit *lineEdit_solde;
    QLineEdit *lineEdit_description;
    QLabel *label_solde;
    QLabel *label_nombreTransactions;
    QLabel *label_tauxInteretMinimum;
    QLabel *label_description;
    QPushButton *pushButton;
    QSpinBox *spinBox_nombreTransactions;
    QDoubleSpinBox *doubleSpinBox_tauxInteret;
    QDoubleSpinBox *doubleSpinBox_tauxInteretMinimum;

    void setupUi(QDialog *AjoutChequeInterfaceClass)
    {
        if (AjoutChequeInterfaceClass->objectName().isEmpty())
            AjoutChequeInterfaceClass->setObjectName(QString::fromUtf8("AjoutChequeInterfaceClass"));
        AjoutChequeInterfaceClass->resize(400, 331);
        label_titre = new QLabel(AjoutChequeInterfaceClass);
        label_titre->setObjectName(QString::fromUtf8("label_titre"));
        label_titre->setGeometry(QRect(100, 10, 211, 17));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_titre->setFont(font);
        lineEdit_noCompte = new QLineEdit(AjoutChequeInterfaceClass);
        lineEdit_noCompte->setObjectName(QString::fromUtf8("lineEdit_noCompte"));
        lineEdit_noCompte->setGeometry(QRect(200, 40, 181, 31));
        label_noCompte = new QLabel(AjoutChequeInterfaceClass);
        label_noCompte->setObjectName(QString::fromUtf8("label_noCompte"));
        label_noCompte->setGeometry(QRect(20, 50, 161, 17));
        QFont font1;
        font1.setPointSize(10);
        label_noCompte->setFont(font1);
        label_tauxInteret = new QLabel(AjoutChequeInterfaceClass);
        label_tauxInteret->setObjectName(QString::fromUtf8("label_tauxInteret"));
        label_tauxInteret->setGeometry(QRect(20, 90, 161, 17));
        label_tauxInteret->setFont(font1);
        lineEdit_solde = new QLineEdit(AjoutChequeInterfaceClass);
        lineEdit_solde->setObjectName(QString::fromUtf8("lineEdit_solde"));
        lineEdit_solde->setGeometry(QRect(200, 120, 181, 31));
        lineEdit_description = new QLineEdit(AjoutChequeInterfaceClass);
        lineEdit_description->setObjectName(QString::fromUtf8("lineEdit_description"));
        lineEdit_description->setGeometry(QRect(200, 240, 181, 31));
        label_solde = new QLabel(AjoutChequeInterfaceClass);
        label_solde->setObjectName(QString::fromUtf8("label_solde"));
        label_solde->setGeometry(QRect(20, 130, 161, 17));
        label_solde->setFont(font1);
        label_nombreTransactions = new QLabel(AjoutChequeInterfaceClass);
        label_nombreTransactions->setObjectName(QString::fromUtf8("label_nombreTransactions"));
        label_nombreTransactions->setGeometry(QRect(20, 170, 161, 17));
        label_nombreTransactions->setFont(font1);
        label_tauxInteretMinimum = new QLabel(AjoutChequeInterfaceClass);
        label_tauxInteretMinimum->setObjectName(QString::fromUtf8("label_tauxInteretMinimum"));
        label_tauxInteretMinimum->setGeometry(QRect(20, 210, 161, 17));
        label_tauxInteretMinimum->setFont(font1);
        label_description = new QLabel(AjoutChequeInterfaceClass);
        label_description->setObjectName(QString::fromUtf8("label_description"));
        label_description->setGeometry(QRect(20, 250, 161, 17));
        label_description->setFont(font1);
        pushButton = new QPushButton(AjoutChequeInterfaceClass);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(160, 290, 85, 27));
        spinBox_nombreTransactions = new QSpinBox(AjoutChequeInterfaceClass);
        spinBox_nombreTransactions->setObjectName(QString::fromUtf8("spinBox_nombreTransactions"));
        spinBox_nombreTransactions->setGeometry(QRect(200, 160, 49, 27));
        doubleSpinBox_tauxInteret = new QDoubleSpinBox(AjoutChequeInterfaceClass);
        doubleSpinBox_tauxInteret->setObjectName(QString::fromUtf8("doubleSpinBox_tauxInteret"));
        doubleSpinBox_tauxInteret->setGeometry(QRect(200, 80, 62, 27));
        doubleSpinBox_tauxInteretMinimum = new QDoubleSpinBox(AjoutChequeInterfaceClass);
        doubleSpinBox_tauxInteretMinimum->setObjectName(QString::fromUtf8("doubleSpinBox_tauxInteretMinimum"));
        doubleSpinBox_tauxInteretMinimum->setGeometry(QRect(200, 200, 62, 27));

        retranslateUi(AjoutChequeInterfaceClass);

        QMetaObject::connectSlotsByName(AjoutChequeInterfaceClass);
    } // setupUi

    void retranslateUi(QDialog *AjoutChequeInterfaceClass)
    {
        AjoutChequeInterfaceClass->setWindowTitle(QApplication::translate("AjoutChequeInterfaceClass", "AjoutChequeInterface", 0, QApplication::UnicodeUTF8));
        label_titre->setText(QApplication::translate("AjoutChequeInterfaceClass", "Ajout d'un compte cheque", 0, QApplication::UnicodeUTF8));
        label_noCompte->setText(QApplication::translate("AjoutChequeInterfaceClass", "Numero de compte :", 0, QApplication::UnicodeUTF8));
        label_tauxInteret->setText(QApplication::translate("AjoutChequeInterfaceClass", "Taux d'interet :", 0, QApplication::UnicodeUTF8));
        label_solde->setText(QApplication::translate("AjoutChequeInterfaceClass", "Solde :", 0, QApplication::UnicodeUTF8));
        label_nombreTransactions->setText(QApplication::translate("AjoutChequeInterfaceClass", "Nombre de transactions :", 0, QApplication::UnicodeUTF8));
        label_tauxInteretMinimum->setText(QApplication::translate("AjoutChequeInterfaceClass", "Taux d'interet minimum :", 0, QApplication::UnicodeUTF8));
        label_description->setText(QApplication::translate("AjoutChequeInterfaceClass", "Description :", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("AjoutChequeInterfaceClass", "Ajouter", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AjoutChequeInterfaceClass: public Ui_AjoutChequeInterfaceClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AJOUTCHEQUEINTERFACE_H
