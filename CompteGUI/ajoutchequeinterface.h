#ifndef AJOUTCHEQUEINTERFACE_H
#define AJOUTCHEQUEINTERFACE_H

#include <QtGui/QDialog>
#include "ui_ajoutchequeinterface.h"

class AjoutChequeInterface : public QDialog
{
    Q_OBJECT

public:
    AjoutChequeInterface(QWidget *parent = 0);
    ~AjoutChequeInterface();
    const QString reqNoCompte() const;
    double reqTauxInteret() const;
    const QString reqSolde() const;
    int reqNombreTransactions() const;
    double reqTauxInteretMinimum() const;
    const QString reqDescription() const;


public slots:
	void valideAjout();

private:
    Ui::AjoutChequeInterfaceClass ui;
};

#endif // AJOUTCHEQUEINTERFACE_H
