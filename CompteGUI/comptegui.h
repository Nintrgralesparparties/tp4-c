#ifndef COMPTEGUI_H
#define COMPTEGUI_H

#include <QtGui/QMainWindow>
#include "ui_comptegui.h"

class CompteGUI : public QMainWindow
{
    Q_OBJECT

public:
    CompteGUI(QWidget *parent = 0);
    ~CompteGUI();
    void ajoutCheque(int p_noCompte, double p_tauxInteret, double p_solde,
			int p_nombreTransactions, double p_tauxInteretMinimum=0.1,
			const std::string& p_description="Cheque");

public slots:
	void dialogAjoutCheque();

private:
    Ui::CompteGUIClass ui;
};

#endif // COMPTEGUI_H
