#include "ajoutchequeinterface.h"
#include <qmessagebox.h>

AjoutChequeInterface::AjoutChequeInterface(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);
	QObject::connect(ui.pushButton, SIGNAL(clicked()), this, SLOT(valideAjout()));
}

AjoutChequeInterface::~AjoutChequeInterface()
{

}

const QString AjoutChequeInterface::reqNoCompte() const
{
	return ui.lineEdit_noCompte->text();
}

double AjoutChequeInterface::reqTauxInteret() const
{
	return ui.doubleSpinBox_tauxInteret->value();
}

const QString AjoutChequeInterface::reqSolde() const
{
	return ui.lineEdit_solde->text();
}

int AjoutChequeInterface::reqNombreTransactions() const
{
	return ui.spinBox_nombreTransactions->value();
}

double AjoutChequeInterface::reqTauxInteretMinimum() const
{
	return ui.doubleSpinBox_tauxInteretMinimum->value();
}

const QString AjoutChequeInterface::reqDescription() const
{
	return ui.lineEdit_description->text();
}

void AjoutChequeInterface::valideAjout()
{

}
