/**
 * \file CompteEpargneTesteur.cpp
 * \brief Implantation des tests unitaires de la classe Epargne
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-03-31
 */

#include <gtest/gtest.h>
#include "Epargne.h"

using namespace std;
using namespace banque;

/**
 * \brief Test du constructeur
 * 		 	Cas valides : vérification d'assignation de tous les attributs.
 * 		  	Cas invalides :
 * 		  		Taux d'intérêt < 0.1
 * 		  		Taux d'intérêt > 3.5
 */
TEST(Epargne, Constructeur)
{
	Epargne unCompteEpargne(2776, 1.8, 7100.35);
	ASSERT_EQ(2776, unCompteEpargne.reqNoCompte());
	ASSERT_EQ(1.8, unCompteEpargne.reqTauxInteret());
	ASSERT_EQ(7100.35, unCompteEpargne.reqSolde());
	ASSERT_EQ("Epargne", unCompteEpargne.reqDescription());
}
//Taux d'intérêt invalide
TEST(Epargne, ConstructeurTauxInteretPlusPetitQue01)
{
	ASSERT_THROW(Epargne unCompteEpargne(2776, -0.1, 7100.35), ContratException);
}
//Taux d'intérêt invalide
TEST(Epargne, ConstructeurTauxInteretPlusGrand35)
{
	ASSERT_THROW(Epargne unCompteEpargne(2776, 3.51, 7100.35), ContratException);
}

/**
 * \brief Création d'une fixture à utiliser pour tester les méthodes public de la classe
 */
class unCompteEpargne: public ::testing::Test
{
public:
	unCompteEpargne():
		compteEpargne(109, 1.7, 9200.00, "étudiant"){}
	Epargne compteEpargne;
};

/**
 * \brief Test de la méthode double calculerInteret() const
 */
TEST_F(unCompteEpargne, calculerInteret)
{
	ASSERT_EQ(156.4, compteEpargne.calculerInteret());
}

/**
 * \brief Test de la méthode std::string reqCompteFormate() const
 */
TEST_F(unCompteEpargne, reqCompteFormate)
{
	ostringstream os;
	os << endl << "Compte Epargne" << endl
			<< "numero                 : " << compteEpargne.reqNoCompte() << endl
			<< "Description            : " << compteEpargne.reqDescription() << endl
			<< "Date d'ouverture       : " << compteEpargne.reqDateOuverture().reqDateFormatee() << endl
			<< "Taux d'interet         : " << compteEpargne.reqTauxInteret() << endl
			<< "Solde                  : " << compteEpargne.reqSolde() << " $" << endl
			<< "Interet                : " << compteEpargne.calculerInteret() << " $";
	ASSERT_EQ(os.str(), compteEpargne.reqCompteFormate());
}

/**
 * \brief Test de la méthode Compte* clone() const
 */
TEST_F(unCompteEpargne, clone)
{
	Epargne* compteEpargneClone = static_cast<Epargne*>(compteEpargne.clone());
	ASSERT_TRUE(compteEpargne.reqNoCompte() == compteEpargneClone->reqNoCompte() &&
			compteEpargne.reqTauxInteret() == compteEpargneClone->reqTauxInteret() &&
			compteEpargne.reqSolde() == compteEpargneClone->reqSolde() &&
			compteEpargne.reqDescription() == compteEpargneClone->reqDescription());
}
