/**
 * \file Epargne.h
 * \brief Fichier de l'interface de la classe Epargne, dérivée de la classe Compte.
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-03-25
 */

#ifndef EPARGNE_H_
#define EPARGNE_H_

#include "Compte.h"

namespace banque
{

/**
 * \class Epargne
 * \brief Cette classe, dérivée de la classe Compte, sert à modéliser les comptes de type Epargne.
 */
class Epargne: public Compte
{
public:
	Epargne(int p_noCompte, double p_tauxInteret, double p_solde, const std::string& p_description="Epargne");

	virtual double calculerInteret() const;
	virtual std::string reqCompteFormate() const;
	virtual Compte* clone() const;

private:
	void verifieInvariant() const;
};

} /* namespace banque */

#endif /* EPARGNE_H_ */
