/**
 * \file Cheque.cpp
 * \brief Implantation de la classe Cheque.
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-04-01
 */

#include "Cheque.h"
#include <sstream>

using namespace std;

namespace banque
{

/**
 * \brief Constructeur de la classe qui construit un objet Cheque à partir de valeurs passées en paramètres.
 * \param[in] p_noCompte est un entier qui représente le numéro de compte.
 * \param[in] p_tauxInteret est un flottant double qui représente le taux d'intérêt du compte.
 * \param[in] p_solde est est un flottant double qui représente le solde du compte.
 * \param[in] p_nombreTransactions est un entier qui représente le nombre de transactions.
 * \param[in] p_tauxInteretMinimum est un flottant double qui représente le taux d'intérêt minimum.
 * \param[in] p_description est une chaîne de caractères qui représente la description du compte.
 * \pre p_tauxInteret doit être plus grand ou égal au taux d'intérêt minimum.
 * \pre p_nombreTransactions doit être positif.
 * \pre p_nombreTransactions ne peut être plus grand que 40.
 * \pre p_tauxInteretMinimum doit être positif.
 * \post m_nombreTransactions prend la valeur de p_nombreTransactions.
 * \post m_tauxInteretMinimum prend la valeur de p_tauxInteretMinimum.
 */
Cheque::Cheque(int p_noCompte, double p_tauxInteret, double p_solde,
		int p_nombreTransactions, double p_tauxInteretMinimum,
		const std::string& p_description):
		Compte(p_noCompte, p_tauxInteret, p_solde, p_description),
		m_nombreTransactions(p_nombreTransactions), m_tauxInteretMinimum(p_tauxInteretMinimum)
{
	PRECONDITION(p_tauxInteret >= p_tauxInteretMinimum);
	PRECONDITION(p_nombreTransactions >= 0);
	PRECONDITION(p_nombreTransactions <= 40);
	PRECONDITION(p_tauxInteretMinimum > 0);
	POSTCONDITION(m_nombreTransactions == p_nombreTransactions);
	POSTCONDITION(m_tauxInteretMinimum == p_tauxInteretMinimum);
	INVARIANTS();
}

/**
 * \brief Retourne le nombre de transactions.
 * \return Un entier qui représente le nombre de transactions.
 */
int Cheque::reqNombreTransactions() const
{
	return m_nombreTransactions;
}

/**
 * \brief Retourne le taux d'intérêt minimum.
 * \return Un flottant double qui représente le taux d'intérêt minimum.
 */
double Cheque::reqTauxInteretMinimum() const
{
	return m_tauxInteretMinimum;
}

/**
 * \brief Assigne un nouveau nombre de transactions au compte courant.
 * \param[in] p_nombreTransactions est un entier qui représente le nouveau nombre de transactions.
 * \pre p_nombreTransactions doit être positif.
 * \pre p_nombreTransactions ne peut être plus grand que 40.
 * \post L'objet a été assigné à partir de l'entier passé en paramètre.
 */
void Cheque::asgNombreTransactions(int p_nombreTransactions)
{
	PRECONDITION(p_nombreTransactions >= 0);
	PRECONDITION(p_nombreTransactions <= 40);

	m_nombreTransactions = p_nombreTransactions;

	POSTCONDITION(reqNombreTransactions() == p_nombreTransactions);
	INVARIANTS();
}

/**
 * \brief Calcule l'intérêt du compte.
 * \return Un flottant double qui représente l'intérêt du compte.
 */
double Cheque::calculerInteret() const
{
	double interet = 0;

	if (reqSolde() < 0.0)
	{
		if (m_nombreTransactions >= 0 && m_nombreTransactions <= 10)
		{
			interet = m_tauxInteretMinimum * (-reqSolde()) / 100;
		}
		if (m_nombreTransactions >= 11 && m_nombreTransactions <= 25)
		{
			interet = 0.4*reqTauxInteret() * (-reqSolde()) / 100;
		}
		if (m_nombreTransactions >= 26 && m_nombreTransactions <= 35)
		{
			interet = 0.8*reqTauxInteret() * (-reqSolde()) / 100;
		}
		if (m_nombreTransactions > 35)
		{
			interet = reqTauxInteret() * (-reqSolde()) / 100;
		}
	}

	return interet;
}

/**
 * \brief Retourne les informations formatées correspondant à un compte.
 * \return Une chaîne de caractères qui représente un ensemble des informations correspondant à un compte.
 */
std::string Cheque::reqCompteFormate() const
{
	ostringstream os;
	os << endl << "Compte Cheque" << endl;
	os << Compte::reqCompteFormate();
	os << "nombre de transactions : " << m_nombreTransactions << endl;
	os << "Taux d'interet minimum : " << m_tauxInteretMinimum << endl;
	os << "Interet                : " << calculerInteret() << " $";
	return os.str();
}

/**
 * \brief Fait une copie allouée sur le monceau de l'objet courant.
 * \return L'objet de type Compte qui représente une copie de l'objet courant.
 */
Compte* Cheque::clone() const
{
	return new Cheque(*this);
}

/**
 * \brief Verifie les invariants de la classe Cheque.
 */
void Cheque::verifieInvariant() const
{
	INVARIANT(reqTauxInteret() >= reqTauxInteretMinimum());
	INVARIANT(reqNombreTransactions() >= 0);
	INVARIANT(reqNombreTransactions() <= 40);
	INVARIANT(reqTauxInteretMinimum() > 0);
}

} /* namespace banque */
