/**
 * \file Epargne.cpp
 * \brief Implantation de la classe Epargne.
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-03-25
 */

#include "Epargne.h"
#include <sstream>

using namespace std;

namespace banque
{

/**
 * \brief Constructeur de la classe qui construit un objet Epargne à partir de valeurs passées en paramètres.
 * \param[in] p_noCompte est un entier qui représente le numéro de compte.
 * \param[in] p_tauxInteret est un flottant double qui représente le taux d'intérêt du compte.
 * \param[in] p_solde est est un flottant double qui représente le solde du compte.
 * \param[in] p_description est une chaîne de caractères qui représente la description du compte.
 * \pre p_tauxInteret doit être dans l’intervalle [0.1, 3.5].
 */
Epargne::Epargne(int p_noCompte, double p_tauxInteret, double p_solde, const std::string& p_description):
		Compte(p_noCompte, p_tauxInteret, p_solde, p_description)
{
	PRECONDITION(p_tauxInteret >= 0.1 && p_tauxInteret <= 3.5);
	INVARIANTS();
}

/**
 * \brief Calcule l'intérêt du compte.
 * \return Un flottant double qui représente l'intérêt du compte.
 */
double Epargne::calculerInteret() const
{
	return reqTauxInteret() * reqSolde() / 100;
}

/**
 * \brief Retourne les informations formatées correspondant à un compte.
 * \return Une chaîne de caractères qui représente un ensemble des informations correspondant à un compte.
 */
std::string Epargne::reqCompteFormate() const
{
	ostringstream os;
	os << endl << "Compte Epargne" << endl;
	os << Compte::reqCompteFormate();
	os << "Interet                : " << calculerInteret() << " $";
	return os.str();
}

/**
 * \brief Fait une copie allouée sur le monceau de l'objet courant.
 * \return L'objet de type Compte qui représente une copie de l'objet courant.
 */
Compte* Epargne::clone() const
{
	return new Epargne(*this);
}

/**
 * \brief Verifie les invariants de la classe Epargne.
 */
void Epargne::verifieInvariant() const
{
	INVARIANT(reqTauxInteret() >= 0.1 && reqTauxInteret() <= 3.5);
}

} /* namespace banque */
