/**
 * \file validationFormat.cpp
 * \brief Fichier qui contient les défintions des fonctions utilitaires.
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-02-12
 */

#include "validationFormat.h"

using namespace std;

namespace util
{

/**
 * \brief Vérifie que le nom ne contient que des lettres d'alphabet latin.
 * \param[in] p_nom est une chaîne de caractères qui représente le nom.
 * \return Un booléen indiquant si le nom est valide.
 */
bool validerFormatNom(const std::string& p_nom)
{
	bool valide = 1;

	if (p_nom.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ") != string::npos)
	{
		valide = 0;
	}

	return valide;
}

/**
 * \brief Vérifie que le numéro de téléphone est le numéro canadien au format national.
 * \param[in] p_telephone est une chaîne de caractères qui représente le numéro de téléphone.
 * \return Un booléen indiquant si le numéro de téléphone est valide.
 */
bool validerTelephone(const std::string& p_telephone)
{
	bool valide = 0;

	string indicatifs[28] = {"403","780","604","236","250","778","902","204","506","902",
							 "905","519","289","705","613","807","416","647","438","514",
							 "450","579","418","581","819","306","709","867"};

	if (p_telephone.length() == 12)
	{
		for (int i = 0; i < 28; i++)
			{
				if (p_telephone.substr(0,3) == indicatifs[i])
				{
					if (p_telephone.substr(3,1) == " ")
					{
						if ((p_telephone.substr(4,3).find_first_not_of("1234567890") == string::npos)
								&& p_telephone.substr(4,3).length() == 3)
						{
							if (p_telephone.substr(7,1) == "-")
							{
								if ((p_telephone.substr(8,4).find_first_not_of("1234567890") == string::npos)
										&& p_telephone.substr(8,4).length() == 4)
								{
									valide = 1;
								}
							}
						}
					}
				}
			}
	}

	return valide;
}

/**
 * \brief Vérifie le format du fichier.
 * \param[in] p_is est un flux qui représente le texte du fichier.
 * \return Un booléen indiquant si le format du fichier est valide.
 */
bool validerFormatFichier(std::istream& p_is)
{
	bool valide = 0;
	int nb_lignes = 0;
	char ligne[256];

	while(p_is.getline(ligne, 255))
	{
		nb_lignes++;
	}

	string liste[nb_lignes+1];
	p_is.clear();
	p_is.seekg(0, ios::beg);

	for (int i = 0; i < nb_lignes; i++)
	{
		p_is.getline(ligne, 255);
		liste[i] = ligne;
	}

	if (!liste[0].empty() && validerFormatNom(liste[0]))
	{
		if (!liste[1].empty() && validerFormatNom(liste[1]))
		{
			istringstream is_chaine(liste[2]);
			int jj;
			is_chaine >> jj;
			int mm;
			is_chaine >> mm;
			int aaaa;
			is_chaine >> aaaa;
			string reste;
			is_chaine >> reste;

			if (jj > 0 && jj < 32 && mm > 0 && mm < 13 && aaaa > 1800 && reste.empty())
			{
				if (validerTelephone(liste[3]))
				{
					int i = 5;
					while (liste[i] == "cheque" || liste[i] == "epargne")
					{
						if (liste[i] == "cheque")
						{
							i = i + 7;
						}
						else
						{
							i = i + 6;
						}
					}
					if (i == nb_lignes)
					{
						valide = 1;
					}
				}
			}
		}
	}

	return valide;
}

} // namespace util
