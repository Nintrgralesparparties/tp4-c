/**
 * \file Compte.cpp
 * \brief Implantation de la classe Compte.
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-03-25
 */

#include "Compte.h"
#include <sstream>

using namespace std;

namespace banque
{

/**
 * \brief Constructeur de la classe qui construit un objet Compte à partir de valeurs passées en paramètres.
 * \param[in] p_noCompte est un entier qui représente le numéro de compte.
 * \param[in] p_tauxInteret est un flottant double qui représente le taux d'intérêt du compte.
 * \param[in] p_solde est est un flottant double qui représente le solde du compte.
 * \param[in] p_description est une chaîne de caractères qui représente la description du compte.
 * \pre p_noCompte doit être positif.
 * \pre p_tauxInteret doit être positif.
 * \pre p_description doit être non vide.
 * \post m_noCompte prend la valeur de p_noCompte.
 * \post m_tauxInteret prend la valeur de p_tauxInteret.
 * \post m_solde prend la valeur de p_solde.
 * \post m_description prend la valeur de p_description.
 */
Compte::Compte(int p_noCompte, double p_tauxInteret, double p_solde, const std::string& p_description):
		m_noCompte(p_noCompte), m_tauxInteret(p_tauxInteret), m_solde(p_solde), m_description(p_description)
{
	PRECONDITION(p_noCompte > 0);
	PRECONDITION(p_tauxInteret > 0);
	PRECONDITION(!p_description.empty());
	POSTCONDITION(m_noCompte == p_noCompte);
	POSTCONDITION(m_tauxInteret == p_tauxInteret);
	POSTCONDITION(m_solde == p_solde);
	POSTCONDITION(m_description == p_description);
	INVARIANTS();
}

/**
 * \brief Retourne le numéro de compte.
 * \return Un entier qui représente le numéro de compte.
 */
int Compte::reqNoCompte() const
{
	return m_noCompte;
}

/**
 * \brief Retourne le taux d'intérêt du compte.
 * \return Un flottant double qui représente le taux d'intérêt du compte.
 */
double Compte::reqTauxInteret() const
{
	return m_tauxInteret;
}

/**
 * \brief Retourne le solde du compte.
 * \return Un flottant double qui représente le solde du compte.
 */
double Compte::reqSolde() const
{
	return m_solde;
}

/**
 * \brief Retourne la description du compte.
 * \return Une chaîne de caractères qui représente la description du compte.
 */
const std::string& Compte::reqDescription() const
{
	return m_description;
}

/**
 * \brief Retourne la date d'ouverture du compte.
 * \return L'objet de type Date qui représente la date d'ouverture du compte.
 */
const util::Date Compte::reqDateOuverture() const
{
	return m_dateOuverture;
}

/**
 * \brief Assigne un nouveau taux d'intérêt au compte courant.
 * \param[in] p_tauxInteret est un flottant double qui représente le nouveau taux d'intérêt du compte.
 * \pre p_tauxInteret doit être positif.
 * \post L'objet a été assigné à partir du flottant double passé en paramètre.
 */
void Compte::asgTauxInteret(double p_tauxInteret)
{
	PRECONDITION(p_tauxInteret > 0);

	m_tauxInteret = p_tauxInteret;

	POSTCONDITION(reqTauxInteret() == p_tauxInteret);
	INVARIANTS();
}

/**
 * \brief Assigne un nouveau solde au compte courant.
 * \param[in] p_solde est un flottant double qui représente le nouveau solde du compte.
 * \post L'objet a été assigné à partir du flottant double passé en paramètre.
 */
void Compte::asgSolde(double p_solde)
{
	m_solde = p_solde;

	POSTCONDITION(reqSolde() == p_solde);
	INVARIANTS();
}

/**
 * \brief Assigne une nouvelle description au compte courant.
 * \param[in] p_description est une chaîne de caractères qui représente la nouvelle description du compte.
 * \pre p_description doit être non vide.
 * \post L'objet a été assigné à partir d'une chaîne de caractères passée en paramètre.
 */
void Compte::asgDescription(const std::string& p_description)
{
	PRECONDITION(!p_description.empty());

	m_description = p_description;

	POSTCONDITION(reqDescription() == p_description);
	INVARIANTS();
}

/**
 * \brief Retourne les informations formatées correspondant à un compte.
 * \return Une chaîne de caractères qui représente un ensemble des informations correspondant à un compte.
 */
std::string Compte::reqCompteFormate() const
{
	ostringstream os;

	os << "numero                 : " << m_noCompte << endl;
	os << "Description            : " << m_description << endl;
	os << "Date d'ouverture       : " << m_dateOuverture.util::Date::reqDateFormatee() << endl;
	os << "Taux d'interet         : " << m_tauxInteret << endl;
	os << "Solde                  : " << m_solde << " $" << endl;

	return os.str();
}

/**
 * \brief Verifie les invariants de la classe Compte.
 */
void Compte::verifieInvariant() const
{
	INVARIANT(m_noCompte > 0);
	INVARIANT(m_tauxInteret > 0);
	INVARIANT(!m_description.empty());
}

} /* namespace banque */

