/**
 * \file Cheque.h
 * \brief Fichier de l'interface de la classe Cheque, dérivée de la classe Compte.
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-04-01
 */

#ifndef CHEQUE_H_
#define CHEQUE_H_

#include "Compte.h"

namespace banque
{

/**
 * \class Cheque
 * \brief Cette classe, dérivée de la classe Compte, sert à modéliser les comptes de type Cheque.
 */
class Cheque: public Compte
{
public:
	Cheque(int p_noCompte, double p_tauxInteret, double p_solde,
			int p_nombreTransactions, double p_tauxInteretMinimum=0.1,
			const std::string& p_description="Cheque");

	int reqNombreTransactions() const;
	double reqTauxInteretMinimum() const;

	void asgNombreTransactions(int nombreTransactions);

	virtual double calculerInteret() const;
	virtual std::string reqCompteFormate() const;
	virtual Compte* clone() const;

private:
	int m_nombreTransactions;
	double m_tauxInteretMinimum;

	void verifieInvariant() const;
};

} /* namespace banque */

#endif /* CHEQUE_H_ */
