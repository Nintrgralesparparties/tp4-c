/**
 * \file validationFormat.h
 * \brief Fichier qui contient les déclarations des fonctions utilitaires.
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-02-12
 */

#ifndef VALIDATIONFORMAT_H_
#define VALIDATIONFORMAT_H_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

namespace util
{

bool validerFormatNom (const std::string& p_nom);
bool validerTelephone(const std::string& p_telephone);
bool validerFormatFichier(std::istream& p_is);

} // namespace util

#endif /* VALIDATIONFORMAT_H_ */
