/**
 * \file CompteException.h
 * \brief Fichier contenant la déclaration de la classe CompteException et de ses héritiers
 * \author Andrey Kalinin, Simon Lizotte
 * \version 1.0
 * \date 2018-04-19
 */

#ifndef COMPTEEXCEPTION_H_
#define COMPTEEXCEPTION_H_

#include <stdexcept>

/**
 * \class CompteException
 * \brief Classe pour la gestion des erreurs liées aux comptes des clients.
 */
class CompteException : public std::runtime_error
{
public:
	CompteException(const std::string& p_raison):
		std::runtime_error(p_raison){}
};

/**
 * \class CompteDejaPresentException
 * \brief Classe pour la gestion des erreurs de l’ajout d’un doublon de compte.
 */
class CompteDejaPresentException : public CompteException
{
public:
	CompteDejaPresentException(const std::string& p_raison):
		CompteException(p_raison){}
};

/**
 * \class CompteAbsentException
 * \brief Classe pour la gestion des erreurs de la tentative d’effacement d’un compte absent.
 */
class CompteAbsentException : public CompteException
{
public:
	CompteAbsentException(const std::string& p_raison):
		CompteException(p_raison){}
};


#endif /* COMPTEEXCEPTION_H_ */
