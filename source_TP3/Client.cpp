/**
 * \file Client.cpp
 * \brief Implantation de la classe Client.
 * \author Andrey Kalinin
 * \version 2.0
 * \date 2018-03-23
 */

#include "Client.h"
#include <sstream>

using namespace std;

namespace banque
{

/**
 * \brief Constructeur de la classe qui construit un objet Client à partir de valeurs passées en paramètres.
 * \param[in] p_noFolio est un entier qui représente le numéro de folio du client.
 * \param[in] p_nom est une chaîne de caractères qui représente le nom du client.
 * \param[in] p_prenom est une chaîne de caractères qui représente le prénom du client.
 * \param[in] p_telephone est une chaîne de caractères qui représente le numéro de téléphone du client.
 * \param[in] p_dateNaissance est un objet de type Date qui représente la date de naissance du client.
 * \pre p_noFolio doit être dans l’intervalle [1000, 10000[.
 * \pre p_nom, p_prenom ne doivent pas être vide.
 * \pre p_nom, p_prenom ne doivent pas contenir que des lettres d'alphabet latin.
 * \pre p_telephone ne doit pas être vide.
 * \pre p_telephone doit être le numéro canadien au format national (ex. 418 123-4567).
 * \post m_noFolio prend la valeur de p_noFolio.
 * \post m_nom prend la valeur de p_nom.
 * \post m_prenom prend la valeur de p_prenom.
 * \post m_telephone prend la valeur de p_telephone.
 * \post m_dateNaissance prend la valeur de p_dateNaissance.
 */
Client::Client(int p_noFolio, const std::string& p_nom, const std::string& p_prenom,
		const std::string& p_telephone, const util::Date& p_dateNaissance):
				m_noFolio(p_noFolio),m_nom(p_nom),m_prenom(p_prenom),m_telephone(p_telephone),
				m_dateNaissance(p_dateNaissance.reqJour(), p_dateNaissance.reqMois(), p_dateNaissance.reqAnnee())
{
	PRECONDITION(p_noFolio >= 1000 && p_noFolio < 10000);
	PRECONDITION(!p_nom.empty());
	PRECONDITION(!p_prenom.empty());
	PRECONDITION(util::validerFormatNom(p_nom));
	PRECONDITION(util::validerFormatNom(p_prenom));
	PRECONDITION(!p_telephone.empty());
	PRECONDITION(util::validerTelephone(p_telephone));
	POSTCONDITION(m_noFolio == p_noFolio);
	POSTCONDITION(m_nom == p_nom);
	POSTCONDITION(m_prenom == p_prenom);
	POSTCONDITION(m_telephone == p_telephone);
	POSTCONDITION(m_dateNaissance == p_dateNaissance);
	INVARIANTS();
}

/**
 * brief Destructeur de la classe
 */
Client::~Client()
{
	for (unsigned int i = 0; i < m_vComptes.size(); i++)
	{
		delete m_vComptes[i];
	}
}

/**
 * \brief Retourne le numéro de folio du client.
 * \return Un entier qui représente le numéro de folio du client.
 */
int Client::reqNoFolio() const
{
	return m_noFolio;
}

/**
 * \brief Retourne le nom du client.
 * \return Une chaîne de caractères qui représente le nom du client.
 */
const std::string& Client::reqNom() const
{
	return m_nom;
}

/**
 * \brief Retourne le prénom du client.
 * \return Une chaîne de caractères qui représente le prénom du client.
 */
const std::string& Client::reqPrenom() const
{
	return m_prenom;
}

/**
 * \brief Retourne le numéro de telephone du client.
 * \return Une chaîne de caractères qui représente le numéro de téléphone du client.
 */
const std::string& Client::reqTelephone() const
{
	return m_telephone;
}

/**
 * \brief Retourne la date de naissance du client.
 * \return L'objet de type Date qui représente la date de naissance du client.
 */
const util::Date Client::reqDateDeNaissance() const
{
	return m_dateNaissance;
}

/**
 * \brief Assigne un nouveau numéro de téléphone au client courant.
 * \param[in] p_telephone est une chaîne de caractères qui représente le nouveau numéro de téléphone du client.
 * \pre p_telephone doit être le numéro canadien au format national (ex. 418 123-4567).
 * \post reqTelephone() retourne la valeur de p_telephone.
 */
void Client::asgTelephone(const std::string& p_telephone)
{
	PRECONDITION(util::validerTelephone(p_telephone));

	m_telephone = p_telephone;

	POSTCONDITION(reqTelephone() == p_telephone);
	INVARIANTS();
}

/**
 * \brief Retourne les informations formatées correspondant à un client.
 * \return Une chaîne de caractères qui représente un ensemble des informations correspondant à un client.
 */
const std::string Client::reqClientFormate() const
{
	ostringstream os;

	os << "Client no de folio : " << m_noFolio << endl;
	os << m_prenom << " " << m_nom << endl;
	os << "Date de naissance : " << m_dateNaissance.util::Date::reqDateFormatee() << endl;
	os << m_telephone << endl;

	return os.str();
}

/**
 * \brief Surcharge de l'opérateur ==
 * \param[in] p_client est un objet de type Client.
 * \return Un booléen indiquant si les deux clients sont égales ou pas.
 */
bool Client::operator ==(const Client& p_client) const
{
	return m_noFolio == p_client.m_noFolio &&
			m_nom == p_client.m_nom &&
			m_prenom == p_client.m_prenom &&
			m_telephone == p_client.m_telephone &&
			m_dateNaissance == p_client.m_dateNaissance;
}

/**
 * \brief Surcharge de l'opérateur <
 * \param[in] p_client est un objet de type Client.
 * \return Un booléen indiquant si le numéro de folio du client passé en paramètre est plus petit que celui du client courant.
 */
bool Client::operator <(const Client& p_client) const
{
	return m_noFolio < p_client.m_noFolio;
}

/**
 * \brief Vérifie si le client a déjà le compte.
 * \param[in] p_noCompte est un entier qui représente le numéro de compte à vérifier.
 * \return Un booléen indiquant si le client possède déjà le compte ayant le numéro passé en paramètre.
 */
bool Client::compteEstDejaPresent(int p_noCompte) const
{
	bool present = 0;

	if (!m_vComptes.empty())
	{
		unsigned int i = 0;

		while ((i < m_vComptes.size()) && (present = 0))
		{
			if (m_vComptes[i]->reqNoCompte() == p_noCompte)
			{
				present = 1;
			}
			i++;
		}
	}

	return present;
}

/**
 * \brief Ajoute un compte au client courant.
 * \param[in] p_nouveauCompte est l'objet de type Compte qui représente un nouveau compte du client.
 * \post Le compte est présent parmi les comptes du client.
 */
void Client::ajouterCompte(const Compte& p_nouveauCompte)
{
	unsigned int nbComptes = m_vComptes.size();

	if (!compteEstDejaPresent(p_nouveauCompte.reqNoCompte()))
	{
		m_vComptes.push_back(p_nouveauCompte.clone());
	}

	POSTCONDITION(m_vComptes.size() == (nbComptes + 1));
	INVARIANTS();
}

/**
 * \brief Retourne les informations sur tous les comptes du client.
 * \return Une chaîne de caractères qui représente un ensemble des informations sur tous les comptes du client.
 */
const std::string Client::reqReleves() const
{
	ostringstream os;

	os << endl << reqClientFormate();

	if (!m_vComptes.empty())
	{
		for (unsigned int i = 0; i < m_vComptes.size(); i++)
		{
			os << m_vComptes[i]->reqCompteFormate();
		}
	}
	else
	{
		os << endl << "***Ce client ne possède pas de comptes***";
	}

	return os.str();
}

/**
 * \brief Verifie les invariants de la classe Client.
 */
void Client::verifieInvariant() const
{
	INVARIANT(m_noFolio >= 1000 && m_noFolio < 10000);
	INVARIANT(!m_nom.empty());
	INVARIANT(!m_prenom.empty());
	INVARIANT(util::validerFormatNom(m_nom));
	INVARIANT(util::validerFormatNom(m_prenom));
	INVARIANT(!m_telephone.empty());
	INVARIANT(util::validerTelephone(m_telephone));
}

} /* namespace banque */

