/**
 * \file Compte.h
 * \brief Fichier qui contient l'interface de la classe de base abstraite Compte qui sert à la modélisation des comptes des clients d'une banque.
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-03-25
 */

#ifndef COMPTE_H_
#define COMPTE_H_

#include "ContratException.h"
#include <string>
#include "Date.h"

namespace banque
{

/**
 * \class Compte
 * \brief Cette classe de base abstraite sert à modéliser tous les types de comptes des clients d'une banque.
 *
 * 	  Attributs : int m_noCompte : Le numéro de compte.
 * 		  		  double m_tauxInteret : Le taux d'intérêt du compte.
 * 		  		  double m_solde : Le solde du compte.
 * 		  		  string m_description : La description du compte.
 * 		  		  Date m_dateOuverture : La date d'ouverture du compte.
 */
class Compte
{
public:
	Compte(int p_noCompte, double p_tauxInteret, double p_solde, const std::string& p_description);
	virtual ~Compte(){};

	int reqNoCompte() const;
	double reqTauxInteret() const;
	double reqSolde() const;
	const std::string& reqDescription() const;
	const util::Date reqDateOuverture() const;

	void asgTauxInteret(double p_tauxInteret);
	void asgSolde(double p_solde);
	void asgDescription(const std::string& p_description);

	virtual std::string reqCompteFormate() const;
	virtual double calculerInteret() const=0;
	virtual Compte* clone() const=0;

private:
	int m_noCompte;
	double m_tauxInteret;
	double m_solde;
	std::string m_description;
	util::Date m_dateOuverture;

	void verifieInvariant() const;
};

} /* namespace banque */

#endif /* COMPTE_H_ */
