/**
 * \file Client.h
 * \brief Fichier qui contient l'interface de la classe Client qui sert à la modélisation des clients d'une banque.
 * \author Andrey Kalinin
 * \version 2.0
 * \date 2018-03-23
 */

#ifndef CLIENT_H_
#define CLIENT_H_

#include "ContratException.h"
#include "validationFormat.h"
#include <string>
#include <vector>
#include "Date.h"
#include "Compte.h"

namespace banque
{

/**
 * \class Client
 * \brief Cette classe sert à modéliser des clients d'une banque.
 *
 * 	  Attributs : int m_noFolio : Le numéro de folio du client.
 * 		  		  string m_nom : Le nom du client.
 * 		  		  string m_prenom : Le prénom du client.
 * 		  		  string m_telephone : Le numéro de téléphone du client.
 * 		  		  Date m_dateNaissance : La date de naissance du client.
 */
class Client
{
public:
	Client(int p_noFolio, const std::string& p_nom, const std::string& p_prenom,
			const std::string& p_telephone, const util::Date& p_dateNaissance);
	~Client();

	int reqNoFolio() const;
	const std::string& reqNom() const;
	const std::string& reqPrenom() const;
	const std::string& reqTelephone() const;
	const util::Date reqDateDeNaissance() const;

	void asgTelephone(const std::string& p_telephone);
	const std::string reqClientFormate() const;

	bool operator==(const Client& p_client) const;
	bool operator<(const Client& p_client) const;

	void ajouterCompte (const Compte& p_nouveauCompte);
	const std::string reqReleves() const;

private:
	int m_noFolio;
	std::string m_nom;
	std::string m_prenom;
	std::string m_telephone;
	util::Date m_dateNaissance;

	std::vector<Compte*> m_vComptes;

	bool compteEstDejaPresent(int p_noCompte) const;

	void verifieInvariant() const;

	Client(const Client& p_client);
	const Client& operator=(const Client& p_client);
};

} /* namespace banque */

#endif /* CLIENT_H_ */
