/**
 * \file ProgrammePrincipal.cpp
 * \brief Fichier du programme principal
 * \author Andrey Kalinin
 * \version 2.0
 * \date 2018-03-31
 */

#include "Client.h"
#include "Compte.h"
#include "Epargne.h"
#include "Cheque.h"
#include "validationFormat.h"
#include "Date.h"
#include "stdlib.h"
#include <iostream>
#include <string>

using namespace std;
using namespace banque;

int main()
{
	cout << "Bienvenue a l'outil << Client >>" << endl;
	cout << "===================================================" << endl;


	int noFolio = 0;
	do
	{
		cout << endl << "Entrez le numéro de folio du client [1000, 10000[ :" << endl;
		cin >> noFolio;
		if (noFolio < 1000 || noFolio >= 10000)
		{
			cout << "Le numéro de folio doit être dans l’intervalle entre 1000 et 10000 exclusivement." << endl;
		}
	}
	while (noFolio < 1000 || noFolio >= 10000);


	string prenom;
	char buffer[256];
	cin.ignore();
	do
	{
		cout << endl << "Entrez le prénom du client :" << endl;
		cin.getline(buffer, 255);
		prenom = buffer;
		if (!util::validerFormatNom(prenom))
		{
			cout << "Le prénom ne doit pas comporter que des lettres d'alphabet latin." << endl;
		}
	}
	while (!util::validerFormatNom(prenom));


	string nom;
	do
	{
		cout << endl << "Entrez le nom du client :" << endl;
		cin.getline(buffer, 255);
		nom = buffer;
		if (!util::validerFormatNom(nom))
		{
			cout << "Le nom ne doit pas comporter que des lettres d'alphabet latin." << endl;
		}
	}
	while (!util::validerFormatNom(nom));


	string telephone;
	do
	{
		cout << endl << "Entrez le numéro de téléphone du client au format XXX CCC-CCCC :" << endl;
		cin.getline(buffer, 255);
		telephone = buffer;
		if (!util::validerTelephone(telephone))
		{
			cout << "Le numéro de téléphone n'est pas valide." << endl;
		}
	}
	while (!util::validerTelephone(telephone));


	string dateNaissance;
	long jour, mois, annee;
	do
	{
		cout << endl << "Entrez la date de naissance du client au format JJMMAAAA" << endl
				<< "(JJ : jour, MM : mois, AAAA : année; ex. 05101986) :" << endl;
		cin.getline(buffer, 255);
		dateNaissance = buffer;
		if (dateNaissance.length() == 8)
		{
			jour = atoi(dateNaissance.substr(0,2).c_str());
			mois = atoi(dateNaissance.substr(2,2).c_str());
			annee = atoi(dateNaissance.substr(4,4).c_str());
		}
		if (!util::Date::validerDate(jour, mois, annee) || dateNaissance.length() != 8)
		{
			cout << "La date de naissance n'est pas valide." << endl;
		}
	}
	while (!util::Date::validerDate(jour, mois, annee) || dateNaissance.length() != 8);

	banque::Client client(noFolio, nom, prenom, telephone, util::Date(jour, mois, annee));


	cout << endl << "Maintenant, on va créer un compte chèque pour le client." << endl;


	int noCompte = 0;
	do
	{
		cout << endl << "Entrez le numéro du compte :" << endl;
		cin >> noCompte;
		if (noCompte <= 0)
		{
			cout << "Le numéro de compte doit être un nombre entier positif." << endl;
		}
	}
	while (noCompte <= 0);


	double tauxInteretMinimum = 0.0;
	do
	{
		cout << endl << "Entrez le taux d'intérêt minimum du compte :" << endl;
		cin >> tauxInteretMinimum;
		if (tauxInteretMinimum <= 0)
		{
			cout << "Le taux d'intérêt minimum doit être un nombre décimal positif." << endl;
		}
	}
	while (tauxInteretMinimum <= 0);


	double tauxInteret = 0.0;
	do
	{
		cout << endl << "Entrez le taux d'intérêt du compte :" << endl;
		cin >> tauxInteret;
		if (tauxInteret < tauxInteretMinimum)
		{
			cout << "Le taux d'intérêt du compte ne peut pas être plus petit que le taux d'intérêt minimum ("
					<< tauxInteretMinimum << ")."<< endl;
		}
	}
	while (tauxInteret < tauxInteretMinimum);


	double solde = 0.0;
	cout << endl << "Entrez le solde du compte :" << endl;
	cin >> solde;


	int nombreTransactions = 0;
	do
	{
		cout << endl << "Entrez le nombre de transactions du compte (max. 40) :" << endl;
		cin >> nombreTransactions;
		if (nombreTransactions < 0 || nombreTransactions > 40)
		{
			cout << "Le nombre de transactions doit être dans l'intervalle [0, 40]."<< endl;
		}
	}
	while (nombreTransactions < 0 || nombreTransactions > 40);


	cin.ignore();
	string description = "Cheque";
	cout << endl << "Entrez la description du compte :" << endl;
	cin.getline(buffer, 255);
	description = buffer;


	banque::Cheque compteCheque(noCompte, tauxInteret, solde, nombreTransactions, tauxInteretMinimum, description);

	client.ajouterCompte(compteCheque);


	cout << endl << "Le compte chèque a été crée. Créons maintenant un compte épargne." << endl;


	noCompte = 0;
	do
	{
		cout << endl << "Entrez le numéro du compte :" << endl;
		cin >> noCompte;
		if (noCompte <= 0)
		{
			cout << "Le numéro de compte doit être un nombre entier positif." << endl;
		}
	}
	while (noCompte <= 0);


	tauxInteret = 0.0;
	do
	{
		cout << endl << "Entrez le taux d'intérêt du compte (min. 0.1, max. 3.5):" << endl;
		cin >> tauxInteret;
		if (tauxInteret < 0.1 || tauxInteret > 3.5)
		{
			cout << "Le taux d'intérêt du compte doit être dans l'intervalle [0.1, 3.5] pour le compte épargne." << endl;
		}
	}
	while (tauxInteret < 0.1 || tauxInteret > 3.5);


	solde = 0.0;
	cout << endl << "Entrez le solde du compte :" << endl;
	cin >> solde;


	cin.ignore();
	description = "Épargne";
	cout << endl << "Entrez la description du compte :" << endl;
	cin.getline(buffer, 255);
	description = buffer;


	banque::Epargne compteEpargne(noCompte, tauxInteret, solde, description);

	client.ajouterCompte(compteEpargne);


	cout << endl << "Le compte épargne a été crée." << endl;


	cout << endl << "Fiche client" << endl;
	cout << "---------------------------------------------------" << endl;
	cout << client.banque::Client::reqReleves() << endl;

	cout << endl << "Fin du programme" << endl;

return 0;

}
