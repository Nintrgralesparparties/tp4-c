/**
 * \file ClientTesteur.cpp
 * \brief Implantation des tests unitaires de la classe Client
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-03-23
 */

#include <gtest/gtest.h>
#include "Client.h"
#include "Compte.h"
#include "Epargne.h"
#include "Cheque.h"

using namespace std;
using namespace banque;

/**
 * \brief Test du constructeur
 * 		 	Cas valides : vérification d'assignation de tous les attributs.
 * 		  	Cas invalides :
 * 		  		Numéro de folio < 1000
 * 		  		Numéro de folio = 10000
 * 		  		Numéro de folio > 10000
 * 		  		Nom vide
 * 		  		Nom avec un chiffre
 * 		  		Prénom vide
 * 		  		Prénom avec un symbole interdit
 * 		  		Numéro de téléphone vide
 * 		  		Numéro de téléphone ne contenant pas le bon nombre de caractères
 * 		  		Numéro de téléphone avec un espace à la place d'un trait d'union
 * 		  		Numéro de téléphone avec un trait d'union à la place d'un espace
 * 		  		Numéro de téléphone ne contenant que des chiffres
 * 		  		Numéro de téléphone avec un code régional invalide
 */
TEST(Client, Constructeur)
{
	Client unClient(5000, "Charlebois", "Robert", "418 111-3333", util::Date(25,06,1974));
	ASSERT_EQ(5000, unClient.reqNoFolio());
	ASSERT_EQ("Charlebois", unClient.reqNom());
	ASSERT_EQ("Robert", unClient.reqPrenom());
	ASSERT_EQ("418 111-3333", unClient.reqTelephone());
	ASSERT_EQ(util::Date(25,06,1974), unClient.reqDateDeNaissance());
}
//Numéro de folio invalide
TEST(Client, ConstructeurNoFolioPlusPetitQue1000)
{
	ASSERT_THROW(Client unClient(999, "Charlebois", "Robert", "418 111-3333", util::Date(25,06,1974)), ContratException);
}
//Numéro de folio invalide
TEST(Client, ConstructeurNoFolioEstEgalA10000)
{
	ASSERT_THROW(Client unClient(10000, "Charlebois", "Robert", "418 111-3333", util::Date(25,06,1974)), ContratException);
}
//Numéro de folio invalide
TEST(Client, ConstructeurNoFolioPlusGrandQue10000)
{
	ASSERT_THROW(Client unClient(10001, "Charlebois", "Robert", "418 111-3333", util::Date(25,06,1974)), ContratException);
}
//Nom invalide
TEST(Client, ConstructeurNomVide)
{
	ASSERT_THROW(Client unClient(5000, "", "Robert", "418 111-3333", util::Date(25,06,1974)), ContratException);
}
//Nom invalide
TEST(Client, ConstructeurNomInvalideAvecChiffre)
{
	ASSERT_THROW(Client unClient(5000, "Charleboi5", "Robert", "418 111-3333", util::Date(25,06,1974)), ContratException);
}
//Prénom invalide
TEST(Client, ConstructeurPrenomVide)
{
	ASSERT_THROW(Client unClient(5000, "Charlebois", "", "418 111-3333", util::Date(25,06,1974)), ContratException);
}
//Prénom invalide
TEST(Client, ConstructeurPrenomInvalideAvecSymbole)
{
	ASSERT_THROW(Client unClient(5000, "Charlebois", "Robert.", "418 111-3333", util::Date(25,06,1974)), ContratException);
}
//Téléphone invalide
TEST(Client, ConstructeurTelephoneVide)
{
	ASSERT_THROW(Client unClient(5000, "Charlebois", "Robert", "", util::Date(25,06,1974)), ContratException);
}
//Téléphone invalide
TEST(Client, ConstructeurTelephoneInvalideNbCaracteres)
{
	ASSERT_THROW(Client unClient(5000, "Charlebois", "Robert", "418 1111-3333", util::Date(25,06,1974)), ContratException);
}
//Téléphone invalide
TEST(Client, ConstructeurTelephoneInvalideEspace)
{
	ASSERT_THROW(Client unClient(5000, "Charlebois", "Robert", "418 111 3333", util::Date(25,06,1974)), ContratException);
}
//Téléphone invalide
TEST(Client, ConstructeurTelephoneInvalideTraitDUnion)
{
	ASSERT_THROW(Client unClient(5000, "Charlebois", "Robert", "418-111-3333", util::Date(25,06,1974)), ContratException);
}
//Téléphone invalide
TEST(Client, ConstructeurTelephoneInvalideQueDesChiffres)
{
	ASSERT_THROW(Client unClient(5000, "Charlebois", "Robert", "4181113333", util::Date(25,06,1974)), ContratException);
}
//Téléphone invalide
TEST(Client, ConstructeurTelephoneInvalideCodeRegional)
{
	ASSERT_THROW(Client unClient(5000, "Charlebois", "Robert", "151 111-3333", util::Date(25,06,1974)), ContratException);
}

/**
 * \brief Création d'une fixture à utiliser pour tester les méthodes public de la classe
 */
class unClient: public ::testing::Test
{
public:
	unClient():
		client(7500, "Leclerc", "Felix", "418 222-0000", util::Date(02,8,1984)){}
	Client client;
};

/**
 * \brief Test de la méthode int reqNoFolio() const
 */
TEST_F(unClient, reqNoFolio)
{
	ASSERT_EQ(7500, client.reqNoFolio());
}

/**
 * \brief Test de la méthode const std::string& reqNom() const
 */
TEST_F(unClient, reqNom)
{
	ASSERT_EQ("Leclerc", client.reqNom());
}

/**
 * \brief Test de la méthode const std::string& reqPrenom() const
 */
TEST_F(unClient, reqPrenom)
{
	ASSERT_EQ("Felix", client.reqPrenom());
}

/**
 * \brief Test de la méthode const std::string& reqTelephone() const
 */
TEST_F(unClient, reqTelephone)
{
	ASSERT_EQ("418 222-0000", client.reqTelephone());
}

/**
 * \brief Test de la méthode const util::Date reqDateDeNaissance() const
 */
TEST_F(unClient, reqDateDeNaissance)
{
	ASSERT_EQ(util::Date(02,8,1984), client.reqDateDeNaissance());
}

/**
 * \brief Test de la méthode asgTelephone(const std::string& p_telephone)
 * 		 	Cas valides : assignation de numéro de téléphone au format valide.
 * 		  	Cas invalides : assignation de numéro de téléphone invalide, soit
 * 		  		Numéro de téléphone ne contenant pas le bon nombre de caractères
 * 		  		Numéro de téléphone avec un espace à la place d'un trait d'union
 * 		  		Numéro de téléphone avec un trait d'union à la place d'un espace
 * 		  		Numéro de téléphone ne contenant que des chiffres
 * 		  		Numéro de téléphone avec un code régional invalide
 */
TEST_F(unClient, asgTelephoneValide)
{
	client.asgTelephone("514 123-4567");
	ASSERT_EQ("514 123-4567", client.reqTelephone());
}
//Téléphone invalide
TEST_F(unClient, asgTelephoneInvalideNbCaracteres)
{
	ASSERT_THROW(client.asgTelephone("514123-4567"), ContratException);
}
//Téléphone invalide
TEST_F(unClient, asgTelephoneInvalideEspace)
{
	ASSERT_THROW(client.asgTelephone("514 123 4567"), ContratException);
}
//Téléphone invalide
TEST_F(unClient, asgTelephoneInvalideTraitDUnion)
{
	ASSERT_THROW(client.asgTelephone("514-123-4567"), ContratException);
}
//Téléphone invalide
TEST_F(unClient, asgTelephoneInvalideQueDesChiffres)
{
	ASSERT_THROW(client.asgTelephone("5141234567"), ContratException);
}
//Téléphone invalide
TEST_F(unClient, asgTelephoneInvalideCodeRegional)
{
	ASSERT_THROW(client.asgTelephone("515 123-4567"), ContratException);
}

/**
 * \brief Test de la méthode const std::string reqClientFormate() const
 */
TEST_F(unClient, reqClientFormate)
{
	std::ostringstream os;
	os << "Client no de folio : " << client.reqNoFolio() << endl
			<< client.reqPrenom() << " " << client.reqNom() << endl
			<< "Date de naissance : " << client.reqDateDeNaissance().reqDateFormatee() << endl
			<< client.reqTelephone() << endl;
	ASSERT_EQ(os.str(), client.reqClientFormate());
}

/**
 * \brief Test de la méthode const std::string reqReleves() const
 */
TEST_F(unClient, reqReleves)
{
	std::ostringstream os;

	Epargne compteEpargne(109, 1.7, 9200.00, "étudiant");
	Cheque compteCheque(1111, 2.2, -500.00, 22, 0.3, "voyage");
	client.ajouterCompte(compteEpargne);
	client.ajouterCompte(compteCheque);

	os << endl << "Client no de folio : " << client.reqNoFolio() << endl
			<< client.reqPrenom() << " " << client.reqNom() << endl
			<< "Date de naissance : " << client.reqDateDeNaissance().reqDateFormatee() << endl
			<< client.reqTelephone() << endl
			<< endl << "Compte Epargne" << endl
			<< "numero                 : " << compteEpargne.reqNoCompte() << endl
			<< "Description            : " << compteEpargne.reqDescription() << endl
			<< "Date d'ouverture       : " << compteEpargne.reqDateOuverture().reqDateFormatee() << endl
			<< "Taux d'interet         : " << compteEpargne.reqTauxInteret() << endl
			<< "Solde                  : " << compteEpargne.reqSolde() << " $" << endl
			<< "Interet                : " << compteEpargne.calculerInteret() << " $"
			<< endl << "Compte Cheque" << endl
			<< "numero                 : " << compteCheque.reqNoCompte() << endl
			<< "Description            : " << compteCheque.reqDescription() << endl
			<< "Date d'ouverture       : " << compteCheque.reqDateOuverture().reqDateFormatee() << endl
			<< "Taux d'interet         : " << compteCheque.reqTauxInteret() << endl
			<< "Solde                  : " << compteCheque.reqSolde() << " $" << endl
			<< "nombre de transactions : " << compteCheque.reqNombreTransactions() << endl
			<< "Taux d'interet minimum : " << compteCheque.reqTauxInteretMinimum() << endl
			<< "Interet                : " << compteCheque.calculerInteret() << " $";

	ASSERT_EQ(os.str(), client.reqReleves());
}
//Le client ne possède pas de comptes
TEST_F(unClient, ReqRelevesPasDeComptes)
{
	std::ostringstream os;

	os << endl << "Client no de folio : " << client.reqNoFolio() << endl
			<< client.reqPrenom() << " " << client.reqNom() << endl
			<< "Date de naissance : " << client.reqDateDeNaissance().reqDateFormatee() << endl
			<< client.reqTelephone() << endl
			<< endl << "***Ce client ne possède pas de comptes***";

	ASSERT_EQ(os.str(), client.reqReleves());
}

/**
 * \brief Test de la méthode void ajouterCompte (const Compte& p_nouveauCompte)
 */
//Ajout d'un compte epargne
TEST_F(unClient, ajouterCompteEpargne)
{
	Epargne compteEpargne(109, 1.7, 9200.00, "étudiant");

	client.ajouterCompte(compteEpargne);

	std::ostringstream os;

	os << endl << "Client no de folio : " << client.reqNoFolio() << endl
				<< client.reqPrenom() << " " << client.reqNom() << endl
				<< "Date de naissance : " << client.reqDateDeNaissance().reqDateFormatee() << endl
				<< client.reqTelephone() << endl
				<< endl << "Compte Epargne" << endl
				<< "numero                 : " << compteEpargne.reqNoCompte() << endl
				<< "Description            : " << compteEpargne.reqDescription() << endl
				<< "Date d'ouverture       : " << compteEpargne.reqDateOuverture().reqDateFormatee() << endl
				<< "Taux d'interet         : " << compteEpargne.reqTauxInteret() << endl
				<< "Solde                  : " << compteEpargne.reqSolde() << " $" << endl
				<< "Interet                : " << compteEpargne.calculerInteret() << " $";

	ASSERT_EQ(os.str(), client.reqReleves());
}
//Ajout d'un compte cheque
TEST_F(unClient, ajouterCompteCheque)
{
	Cheque compteCheque(1111, 2.2, -500.00, 22, 0.3, "voyage");

	client.ajouterCompte(compteCheque);

	std::ostringstream os;

	os << endl << "Client no de folio : " << client.reqNoFolio() << endl
				<< client.reqPrenom() << " " << client.reqNom() << endl
				<< "Date de naissance : " << client.reqDateDeNaissance().reqDateFormatee() << endl
				<< client.reqTelephone() << endl
				<< endl << "Compte Cheque" << endl
				<< "numero                 : " << compteCheque.reqNoCompte() << endl
				<< "Description            : " << compteCheque.reqDescription() << endl
				<< "Date d'ouverture       : " << compteCheque.reqDateOuverture().reqDateFormatee() << endl
				<< "Taux d'interet         : " << compteCheque.reqTauxInteret() << endl
				<< "Solde                  : " << compteCheque.reqSolde() << " $" << endl
				<< "nombre de transactions : " << compteCheque.reqNombreTransactions() << endl
				<< "Taux d'interet minimum : " << compteCheque.reqTauxInteretMinimum() << endl
				<< "Interet                : " << compteCheque.calculerInteret() << " $";

	ASSERT_EQ(os.str(), client.reqReleves());
}

/**
 * \brief Création d'une fixture à utiliser pour tester les comparaisons de Clients
 */
class TroisClients: public ::testing::Test
{
public:
	TroisClients():
		client1(1523, "Vigneault", "Gilles", "418 111-9876", util::Date(27,10,1978)),
		client2(1524, "Levesque", "Raymond", "418 000-9999", util::Date(07,10,1988)),
		client3(1523, "Vigneault", "Gilles", "418 111-9876", util::Date(27,10,1978)){}
	Client client1;
	Client client2;
	Client client3;
};

/**
 * \brief Test de l'opérateur surchargé bool operator==(const Client& p_client) const
 */
TEST_F(TroisClients, OperateurEgalEgalVrai)
{
	ASSERT_EQ(client1, client3);
}

TEST_F(TroisClients, OperateurEgalEgalFaux)
{
	ASSERT_FALSE(client1 == client2);
}

/**
 * \brief Test de l'opérateur surchargé bool operator<(const Client& p_client) const
 */
TEST_F(TroisClients, OperateurPlusPetitVrai)
{
	ASSERT_TRUE(client3 < client2);
}

TEST_F(TroisClients, OperateurPlusPetitFaux)
{
	ASSERT_FALSE(client2 < client3);
}
