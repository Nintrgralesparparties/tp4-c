/**
 * \file CompteChequeTesteur.cpp
 * \brief Implantation des tests unitaires de la classe Cheque
 * \author Andrey Kalinin
 * \version 1.0
 * \date 2018-04-01
 */

#include <gtest/gtest.h>
#include "Cheque.h"

using namespace std;
using namespace banque;

/**
 * \brief Test du constructeur
 * 		 	Cas valides : vérification d'assignation de tous les attributs.
 * 		  	Cas invalides :
 * 		  		Taux d'intérêt < taux d'intérêt minimum
 * 		  		Nombre de transactions < 0
 * 		  		Nombre de transactions > 40
 * 		  		Taux d'intérêt minimum = 0
 * 		  		Taux d'intérêt minimum < 0
 */
TEST(Cheque, Constructeur)
{
	Cheque unCompteCheque(5555, 3.0, -100.35, 15);
	ASSERT_EQ(5555, unCompteCheque.reqNoCompte());
	ASSERT_EQ(3.0, unCompteCheque.reqTauxInteret());
	ASSERT_EQ(-100.35, unCompteCheque.reqSolde());
	ASSERT_EQ(15, unCompteCheque.reqNombreTransactions());
	ASSERT_EQ(0.1, unCompteCheque.reqTauxInteretMinimum());
	ASSERT_EQ("Cheque", unCompteCheque.reqDescription());
}
//Taux d'intérêt invalide
TEST(Cheque, ConstructeurTauxInteretPlusPetitQueTauxInteretMin)
{
	ASSERT_THROW(Cheque unCompteCheque(5555, 0.9, -100.35, 15, 1.0), ContratException);
}
//Nombre de transactions invalide
TEST(Cheque, ConstructeurNbTransactionsNegatif)
{
	ASSERT_THROW(Cheque unCompteCheque(5555, 3.0, -100.35, -1), ContratException);
}
//Nombre de transactions invalide
TEST(Cheque, ConstructeurNbTransactionsPlusGrandQue40)
{
	ASSERT_THROW(Cheque unCompteCheque(5555, 3.0, -100.35, 41), ContratException);
}
//Taux d'intérêt minimum invalide
TEST(Cheque, ConstructeurTauxInteretMinNul)
{
	ASSERT_THROW(Cheque unCompteCheque(5555, 3.0, -100.35, 15, 0.0), ContratException);
}
//Taux d'intérêt minimum invalide
TEST(Cheque, ConstructeurTauxInteretMinNegatif)
{
	ASSERT_THROW(Cheque unCompteCheque(5555, 3.0, -100.35, 15, -0.1), ContratException);
}

/**
 * \brief Création d'une fixture à utiliser pour tester les méthodes public de la classe
 */
class unCompteCheque: public ::testing::Test
{
public:
	unCompteCheque():
		compteCheque(1111, 2.2, -500.00, 22, 0.3, "voyage"){}
	Cheque compteCheque;
};

/**
 * \brief Test de la méthode int reqNombreTransactions() const
 */
TEST_F(unCompteCheque, reqNombreTransactions)
{
	ASSERT_EQ(22, compteCheque.reqNombreTransactions());
}

/**
 * \brief Test de la méthode double reqTauxInteretMinimum() const
 */
TEST_F(unCompteCheque, reqTauxInteretMinimum)
{
	ASSERT_EQ(0.3, compteCheque.reqTauxInteretMinimum());
}

/**
 * \brief Test de la méthode void asgNombreTransactions(int nombreTransactions)
 * 		 	Cas valides : assignation de nombre de transactions valide.
 * 		  	Cas invalides : nombre de transactions négatif, nombre de transactions plus grand que 40.
 */
TEST_F(unCompteCheque, asgNombreTransactionsValide)
{
	compteCheque.asgNombreTransactions(5);
	ASSERT_EQ(5, compteCheque.reqNombreTransactions());
}
//Nombre de transactions invalide
TEST_F(unCompteCheque, asgNombreTransactionsNegatif)
{
	ASSERT_THROW(compteCheque.asgNombreTransactions(-5), ContratException);
}
//Nombre de transactions invalide
TEST_F(unCompteCheque, asgNombreTransactionsPlusGrandQue40)
{
	ASSERT_THROW(compteCheque.asgNombreTransactions(50), ContratException);
}

/**
 * \brief Test de la méthode double calculerInteret() const
 */
//Nombre de transactions est dans l'intervalle [11, 25]
TEST_F(unCompteCheque, calculerInteretNbTransactions11_25)
{
	ASSERT_EQ(4.4, compteCheque.calculerInteret());
}
//Nombre de transactions est dans l'intervalle [0, 10]
TEST_F(unCompteCheque, calculerInteretNbTransactions0_10)
{
	compteCheque.asgNombreTransactions(4);
	ASSERT_EQ(1.5, compteCheque.calculerInteret());
}
//Nombre de transactions est dans l'intervalle [26, 35]
TEST_F(unCompteCheque, calculerInteretNbTransactions26_35)
{
	compteCheque.asgNombreTransactions(31);
	ASSERT_EQ(8.8, compteCheque.calculerInteret());
}
//Nombre de transactions est dans l'intervalle [36, 40]
TEST_F(unCompteCheque, calculerInteretNbTransactions36_40)
{
	compteCheque.asgNombreTransactions(40);
	ASSERT_EQ(11.0, compteCheque.calculerInteret());
}

/**
 * \brief Test de la méthode std::string reqCompteFormate() const
 */
TEST_F(unCompteCheque, reqCompteFormate)
{
	ostringstream os;
	os << endl << "Compte Cheque" << endl
			<< "numero                 : " << compteCheque.reqNoCompte() << endl
			<< "Description            : " << compteCheque.reqDescription() << endl
			<< "Date d'ouverture       : " << compteCheque.reqDateOuverture().reqDateFormatee() << endl
			<< "Taux d'interet         : " << compteCheque.reqTauxInteret() << endl
			<< "Solde                  : " << compteCheque.reqSolde() << " $" << endl
			<< "nombre de transactions : " << compteCheque.reqNombreTransactions() << endl
			<< "Taux d'interet minimum : " << compteCheque.reqTauxInteretMinimum() << endl
			<< "Interet                : " << compteCheque.calculerInteret() << " $";
	ASSERT_EQ(os.str(), compteCheque.reqCompteFormate());
}

/**
 * \brief Test de la méthode Compte* clone() const
 */
TEST_F(unCompteCheque, clone)
{
	Cheque* compteChequeClone = static_cast<Cheque*>(compteCheque.clone());
	ASSERT_TRUE(compteCheque.reqNoCompte() == compteChequeClone->reqNoCompte() &&
			compteCheque.reqTauxInteret() == compteChequeClone->reqTauxInteret() &&
			compteCheque.reqSolde() == compteChequeClone->reqSolde() &&
			compteCheque.reqNombreTransactions() == compteChequeClone->reqNombreTransactions() &&
			compteCheque.reqTauxInteretMinimum() == compteChequeClone->reqTauxInteretMinimum() &&
			compteCheque.reqDescription() == compteChequeClone->reqDescription());
}
